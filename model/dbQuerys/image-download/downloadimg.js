var log = require("../../../logs/log");
var fs = require('fs');
var https = require('https');
var http = require('http');
var request = require('request');

function Download_image(src,path,useId) {
    return new Promise(function (resolve, reject) {
        var imagesUrlPath = src.split(":");
        var gUrl = imagesUrlPath[0]+"://"+imagesUrlPath[1];
        if (imagesUrlPath[0] === 'https') {
            https.get(src, function (response) {
                var imageFormat = "";
                for(var i=0;i<response.rawHeaders.length;i++){
                    if(response.rawHeaders[i] == 'Content-Type'){
                        if(response.rawHeaders[i+1] == "image/png" || response.rawHeaders[i+1] == "image/jpg" || response.rawHeaders[i+1] == "image/jpeg") {
                            imageFormat = response.rawHeaders[i + 1].split("/");
                            break;
                        }
                    }
                }
                if(imageFormat[1] == 'png' || imageFormat[1] == 'jpg' || imageFormat[1] == 'jpeg') {
                    var random = Math.round(Math.random() * 1299999);
                    var image_name = random + '-' + Date.now() + "." + imageFormat[1];
                    var fileRandname = ""+path+""+image_name+"";
                    var file = fs.createWriteStream(fileRandname);
                    // response.pipe(file);
                    var stream = response.pipe(file);
                    stream.on('finish', function () {
                        var name =image_name.split("-");
                        var format=name[1].split('.');
                        var file =  [{fieldname: 'img',
                            originalname: name[1],
                            encoding: '7bit',
                            mimetype: 'image/'+format[1]+'',
                            destination: './public/images/passport/',
                            filename: image_name,
                            path: 'public\\images\\passport\\'+image_name+'',
                            size: ''}];
                        resolve({error:false,img:image_name,file:file})
                    });

                }else{
                    var errorData = {
                        src:src,
                        path:path,
                        useId:typeof useId != "undefined" ? useId : "no user",
                        date:new Date()
                    };
                    log.insertLog(JSON.stringify(errorData),"./logs/favorit.txt");
                    resolve({error:true})
                }
            });
        }else if(imagesUrlPath[0] === 'http'){
            http.get(gUrl, function (response) {
                var imageFormat = "";
                for(var i=0;i<response.rawHeaders.length;i++){
                    if(response.rawHeaders[i] == 'Content-Type'){
                        if(response.rawHeaders[i+1] == "image/png" || response.rawHeaders[i+1] == "image/jpg" || response.rawHeaders[i+1] == "image/jpeg") {
                            imageFormat = response.rawHeaders[i + 1].split("/");
                            break;
                        }
                    }
                }
                if(imageFormat[1] == 'png' || imageFormat[1] == 'jpg' || imageFormat[1] == 'jpeg') {
                    var random = Math.round(Math.random() * 1299999);
                    var image_name = random + '-' + Date.now() + "." + imageFormat[1];
                    var fileRandname = ""+path+""+image_name+"";
                    console.log('fileRandname AAAAAAAAAAAAAAAAA',fileRandname)
                    var file = fs.createWriteStream(fileRandname);
                    console.log('file BBBBBBBBBBBBBBB',file)
                    response.pipe(file);
                    resolve({error:false,img:image_name})
                }else{
                    var errorData = {
                        src:src,
                        path:path,
                        useId:typeof useId != "undefined" ? useId : "no user",
                        date:new Date()
                    };
                    resolve({error:true})
                }
            });
        }
    })
}


function Download_image_rekurs(num,src,path,useId,result,cb) {
        if(num !== src.length){
            var imagesUrlPath = src[num].split(":");
            if (imagesUrlPath[0] === 'https') {
                https.get(src[num], function (response) {
                    var imageFormat = "";
                    for(var i=0;i<response.rawHeaders.length;i++){
                        if(response.rawHeaders[i] == 'Content-Type'){
                            if(response.rawHeaders[i+1] == "image/png" || response.rawHeaders[i+1] == "image/jpg" || response.rawHeaders[i+1] == "image/jpeg") {
                                imageFormat = response.rawHeaders[i + 1].split("/");
                                break;
                            }
                        }
                    }
                    if(imageFormat[1] == 'png' || imageFormat[1] == 'jpg' || imageFormat[1] == 'jpeg') {
                        var random = Math.round(Math.random() * 1299999);
                        var image_name = random + '-' + Date.now() + "." + imageFormat[1];
                        var fileRandname = ""+path+""+image_name+"";
                        var file = fs.createWriteStream(fileRandname);
                        var stream = response.pipe(file);
                        stream.on('finish', function () {
                            var name =image_name.split("-");
                            var format=name[1].split('.');
                            var file =  [{fieldname: 'img',
                                originalname: name[1],
                                encoding: '7bit',
                                mimetype: 'image/'+format[1]+'',
                                destination: './public/images/product_pending/',
                                filename: image_name,
                                path: 'public\\images\\product_pending\\'+image_name+'',
                                size: ''}];
                            // resolve({error:false,img:image_name,file:file})
                            result.push(file[0]);
                            num++;
                            Download_image_rekurs(num,src,path,useId,result,cb)
                        });

                    }else{
                        var errorData = {
                            src:src,
                            path:path,
                            useId:typeof useId != "undefined" ? useId : "no user",
                            date:new Date()
                        };
                        log.insertLog(JSON.stringify(errorData),"./logs/favorit.txt");
                        cb({error:true,msg:'https'})
                    }
                });
            }else if(imagesUrlPath[0] === 'http'){
                http.get(src[num], function (response) {
                    var imageFormat = "";
                    for(var i=0;i<response.rawHeaders.length;i++){
                        if(response.rawHeaders[i] == 'Content-Type'){
                            if(response.rawHeaders[i+1] == "image/png" || response.rawHeaders[i+1] == "image/jpg" || response.rawHeaders[i+1] == "image/jpeg") {
                                imageFormat = response.rawHeaders[i + 1].split("/");
                                break;
                            }
                        }
                    }
                    if(imageFormat[1] == 'png' || imageFormat[1] == 'jpg' || imageFormat[1] == 'jpeg') {
                        var random = Math.round(Math.random() * 1299999);
                        var image_name = random + '-' + Date.now() + "." + imageFormat[1];
                        var fileRandname = ""+path+""+image_name+"";
                        var file = fs.createWriteStream(fileRandname);
                        response.pipe(file);
                        result.push(file[0]);
                        num++;
                        Download_image_rekurs(num,src,path,useId,result,cb);
                    }else{
                        var errorData = {
                            src:src,
                            path:path,
                            useId:typeof useId != "undefined" ? useId : "no user",
                            date:new Date()
                        };
                        log.insertLog(JSON.stringify(errorData),"./logs/favorit.txt");
                        cb({error:true,msg:'http'})
                    }
                });
            }
        }else{
            cb({error:false,result:result})
        }
}

module.exports.Download_image = Download_image;
module.exports.Download_image_rekurs = Download_image_rekurs;